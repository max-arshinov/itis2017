﻿using FormBuilderWebApp.Services.Tags;

namespace FormBuilderWebApp.Services.Factories
{
   public class InputTagFactory : TagFactory
    {
        public InputTagFactory(Attributes attributes):base(attributes,null)
        {

        }

        public override Tag Create() => new InputTag(Attributes, InnerText);
    }
}
