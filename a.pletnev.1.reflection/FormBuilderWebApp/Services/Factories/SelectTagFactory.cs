﻿using FormBuilderWebApp.Services.Tags;

namespace FormBuilderWebApp.Services.Factories
{
    public class SelectTagFactory : TagFactory
    {
        public SelectTagFactory(Attributes attributes,string innerText):base(attributes, innerText)
        {

        }

        public override Tag Create() => new SelectTag(Attributes,InnerText);
    }
}
