﻿using FormBuilderWebApp.Services.Tags;

namespace FormBuilderWebApp.Services.Factories
{
    public class LabelTagFactory:TagFactory
    {
        public LabelTagFactory(string innerText):base(innerText)
        {

        }

        public override Tag Create() => new LabelTag(InnerText);
    }
}
