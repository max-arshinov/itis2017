﻿using FormBuilderWebApp.Services.Tags;

namespace FormBuilderWebApp.Services.Factories
{
    public abstract class TagFactory
    {
        protected Attributes Attributes { get; set; }

        protected string InnerText { get; set; }

        protected TagFactory(string innerText)
        {
            InnerText = innerText;
        }

        protected TagFactory(Attributes attributes, string innerText) : this(innerText)
        {
            Attributes = attributes;
        }

        public abstract Tag Create();
    }
}
