﻿using FormBuilderWebApp.Services.Tags;

namespace FormBuilderWebApp.Services.Factories
{
    public class OptionTagFactory:TagFactory
    {
        public OptionTagFactory(Attributes attributes, string innerText) : base(attributes, innerText)
        {

        }

        public override Tag Create() => new OptionTag(Attributes, InnerText);
    }
}
