﻿using System.Reflection;

namespace FormBuilderWebApp.Services
{
    public class Property
    {
        public object ViewModel;
        public PropertyInfo PropertyInfo;
    }
}
