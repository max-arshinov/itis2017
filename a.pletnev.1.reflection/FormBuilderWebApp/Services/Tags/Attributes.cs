﻿namespace FormBuilderWebApp.Services.Tags
{
    public class Attributes
    {
        public string Id { get; }

        public string Name { get; }

        public string Type { get; }

        public int? MaxLength { get; }

        public string Value { get; }

        public Attributes(string value="" ,string name="", string id="" , string type = "", int? maxLength = null)
        {
            Name = name;
            Id = id;
            Type = type;
            MaxLength = maxLength;
            Value = value;
        }
    }
}
