﻿namespace FormBuilderWebApp.Services.Tags
{
    public abstract class Tag
    {
        protected string InnerText { get; }

        protected Attributes Attributes { get; }

        protected Tag(string innerText,Attributes attributes)
        {
            Attributes = attributes;
            InnerText = innerText;
        }
    }
}
