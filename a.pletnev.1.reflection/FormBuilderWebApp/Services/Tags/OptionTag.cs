﻿namespace FormBuilderWebApp.Services.Tags
{
    public class OptionTag:Tag
    {
        public OptionTag(Attributes attributes, string innerText) : base(innerText, attributes)
        {
        }

        public override string ToString()
        {
            return $@"<option value=""{Attributes.Value}"">{InnerText}</option>";
        }
    }
}
