﻿namespace FormBuilderWebApp.Services.Tags
{
    public class LabelTag : Tag
    {
        public LabelTag(string innerText) : base(innerText, null)
        {

        }

        public LabelTag(string innerText, Attributes attributes) : base(innerText, attributes)
        {

        }

        public override string ToString()
        {
            return $@"<label>{InnerText}</label>";
        }
    }
}
