﻿namespace FormBuilderWebApp.Services.Tags
{
    public class SelectTag : Tag
    {
        public SelectTag(Attributes attributes, string innerText) : base(innerText,attributes)
        {

        }

        public override string ToString()
        {
            return $@"<select id=""{Attributes.Id}"" name=""{Attributes.Name}"">{InnerText}</select>";
        }
    }
}
