﻿namespace FormBuilderWebApp.Services.Tags
{
    public class InputTag : Tag
    {
        public InputTag(Attributes attributes, string innerText) : base(innerText, attributes)
        {

        }

        public override string ToString()
        {
            return $@"<input type=""{Attributes.Type}"" value=""{Attributes.Value}"" maxlength=""{Attributes.MaxLength}""\>";
        }
    }
}
