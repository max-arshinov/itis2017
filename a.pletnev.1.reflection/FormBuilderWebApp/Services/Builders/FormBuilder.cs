﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FormBuilderWebApp.Services.Builders
{
    public class FormBuilder
    {
        private readonly object _viewModel;
        private readonly string _action;
        private readonly string _controller;
        private readonly string _formMethod;

        private readonly StringBuilder _sb;

        public FormBuilder(object viewModel, string action = "", string controller = "", string formMethod = "post")
        {
            _viewModel = viewModel;
            _action = action;
            _controller = controller;
            _formMethod = formMethod;

            _sb = new StringBuilder();
        }

        public string Build()
        {
            _sb.AppendLine($@"<form action=""/{_controller}/{_action}"" method=""{_formMethod}"">");
            _sb.AppendLine("");

            var viewHandler = new ViewModelHandler(new StringBuilder(), new HashSet<object>());

            viewHandler.AddType(typeof(int),viewHandler.SimpleTypeHandle);
            viewHandler.AddType(typeof(decimal), viewHandler.SimpleTypeHandle);
            viewHandler.AddType(typeof(double), viewHandler.SimpleTypeHandle);
            viewHandler.AddType(typeof(bool), viewHandler.SimpleTypeHandle);

            viewHandler.AddType(typeof(Enum), viewHandler.EnumTypeHandle);

            // Object для пользовательских типов.
            viewHandler.AddType(typeof(object), viewHandler.CustomTypeHandle);

            viewHandler.AddType(typeof(string), viewHandler.ReferenceTypeHandle);

            var form = viewHandler.Execute(_viewModel, new ConcreteSwitchBuilder());

            _sb.AppendLine(form);

            _sb.AppendLine("</form>");
            
            return _sb.ToString();
        }

    }
}
