﻿using System;

namespace FormBuilderWebApp.Services.Builders
{
    public abstract class SwitchBuilder
    {
        protected readonly CustomSwitch CustomSwitch;

        protected SwitchBuilder()
        {
            CustomSwitch = new CustomSwitch();
        }

        public abstract void AddCase(Type type, Action<Property> action);

        public abstract CustomSwitch GetSwitch();
    }
}
