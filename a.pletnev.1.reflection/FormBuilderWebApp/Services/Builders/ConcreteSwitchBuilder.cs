﻿using System;

namespace FormBuilderWebApp.Services.Builders
{
    public class ConcreteSwitchBuilder : SwitchBuilder
    {
        public override void AddCase(Type type,Action<Property> action)
        {
            CustomSwitch.Case(type, action);
        }

        public override CustomSwitch GetSwitch() => CustomSwitch;
    }
}
