﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using FormBuilderWebApp.Services.Builders;
using FormBuilderWebApp.Services.Factories;
using FormBuilderWebApp.Services.Tags;

namespace FormBuilderWebApp.Services
{
    /// <summary>
    /// Обработчик модели.
    /// </summary>
    public class ViewModelHandler
    {
        private CustomSwitch _customSwitch;
        private readonly Dictionary<Type, int> _lengths;

        public ViewModelHandler(StringBuilder sb, HashSet<object> hashTable)
        {
            _hashTable = hashTable;
            _sb = sb;
            _typeHandlers = new List<KeyValuePair<Type, Action<Property>>>();
            _lengths = new Dictionary<Type, int>();
        }

        private readonly HashSet<object> _hashTable;
        private readonly StringBuilder _sb;
        private readonly List<KeyValuePair<Type, Action<Property>>> _typeHandlers;

        public void ReferenceTypeHandle(Property property)
        {
            object value = null;

            if (!(property.ViewModel is Type))
                value = property.PropertyInfo.GetValue(property.ViewModel);

            Appender(new InputTagFactory(new Attributes(value: value?.ToString() ?? string.Empty, type: "text")));
        }

        public void CustomTypeHandle(Property property)
        {
            var propType = property.PropertyInfo.PropertyType;

            var value = property.PropertyInfo.GetValue(property.ViewModel);

            if (_hashTable.Contains(value))
                throw new InvalidOperationException("Зацикливание:повторная встречая того же объекта или пустого типа");

            ExecuteRecursive(value ?? propType);
        }

        public void EnumTypeHandle(Property property)
        {
            var enumValues = Enum.GetValues(property.PropertyInfo.PropertyType);

            var options = GetOptions(enumValues);

            var propName = property.PropertyInfo.Name;

            Appender(new SelectTagFactory(new Attributes(name: propName, id: propName), options));
        }

        public void SimpleTypeHandle(Property property)
        {
            object value = null;

            if (!(property.ViewModel is Type))
                value = property.PropertyInfo.GetValue(property.ViewModel);

            int? maxLength;
            string type;

            var propType = property.PropertyInfo.PropertyType;

            // Определение максимальной длины и типа input.
            switch (propType.Name)
            {
                case "Boolean":
                    maxLength = null;
                    type = "checkbox";
                    break;
                case "Decimal":
                    maxLength = 39;
                    type = "number";
                    break;
                default:
                    if (!_lengths.ContainsKey(propType))
                        _lengths.Add(propType, (int)property.PropertyInfo.PropertyType.GetField("MaxValue").GetRawConstantValue());
                    maxLength = _lengths[propType];
                    type = "number";
                    break;
            }

            Appender(new InputTagFactory(new Attributes(value: value?.ToString() ?? string.Empty, type: type, maxLength: maxLength)));
        }

        public string Execute(object viewModel, SwitchBuilder switchBuilder)
        {
            _customSwitch = ConstructSwitch(switchBuilder);
            return ExecuteRecursive(viewModel);
        }

        public void AddType(Type type, Action<Property> handler)
        {
            _typeHandlers.Add(new KeyValuePair<Type, Action<Property>>(type, handler));
        }

        private string ExecuteRecursive(object viewModel)
        {
            if (viewModel == null)
                throw new NullReferenceException("Модель не можеть быть null");

            _hashTable.Add(viewModel);

            var properties = viewModel is Type type ? type.GetProperties() : viewModel.GetType().GetProperties();

            if (properties.Length == 0)
                return "";

            foreach (var property in properties)
            {
                var propName = GetName(property);

                Appender(new LabelTagFactory(propName));

                Type propertyType;

                if (property.PropertyType.IsEnum)
                    propertyType = typeof(Enum);
                else if (!property.PropertyType.IsSealed && !property.PropertyType.IsPrimitive)
                    propertyType = typeof(object);
                else
                    propertyType = property.PropertyType;

                _customSwitch.Switch(propertyType, new Property() { PropertyInfo = property, ViewModel = viewModel });
            }

            return _sb.ToString();
        }

        private string GetName(MemberInfo property)
        {
            var attr = (DisplayNameAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayNameAttribute));

            return attr == null ? property.Name : attr.DisplayName;
        }

        private string GetOptions(IEnumerable enumValues)
        {
            var options = "\n";

            foreach (var enumValue in enumValues)
            {
                var enumValStr = enumValue.ToString();

                var option = new OptionTagFactory(new Attributes(value: enumValStr), enumValStr).Create();

                options += "    " + "    " + option + "\n";
            }

            return options;
        }

        private void Appender(TagFactory factory)
        {
            var tag = factory.Create();

            _sb.AppendLine(tag.ToString());
        }

        private CustomSwitch ConstructSwitch(SwitchBuilder builder)
        {
            foreach (var typeHandler in _typeHandlers)
            {
                builder.AddCase(typeHandler.Key, typeHandler.Value);
            }

            return builder.GetSwitch();
        }
    }
}
