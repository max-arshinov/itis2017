﻿using System;
using System.Collections.Generic;

namespace FormBuilderWebApp.Services
{
    /// <summary>
    /// Альтернатива свича для типов.
    /// </summary>
    public class CustomSwitch
    {
        private readonly Dictionary<Type, Action<Property>> _matches = new Dictionary<Type, Action<Property>>();

        public CustomSwitch Case(Type type, Action<Property> action)
        {
            _matches.Add(type, action);
            return this;
        }

        public void Switch(Type type, Property parameters)
        {
            _matches[type](parameters);
        }
    }
}
