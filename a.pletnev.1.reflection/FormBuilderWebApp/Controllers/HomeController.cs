﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FormBuilderWebApp.Models;
using FormBuilderWebApp.Services;
using FormBuilderWebApp.Services.Builders;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ViewComponents;

namespace FormBuilderWebApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var testViewModel = new TestViewModel(1, "Shrek"
                , new TestViewModel2("Cat in boots", new TestViewModel3("Fiona"))
                , "It is my SWAAAAAMPPPPP!!!");

            var formBuilder = new FormBuilder(testViewModel, "Index", "Home", "Post");
            var htmlResult = formBuilder.Build();

            return Content(htmlResult, "text/html", Encoding.UTF8);
        }
    }
}