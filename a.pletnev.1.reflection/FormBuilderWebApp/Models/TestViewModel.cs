﻿using System.ComponentModel;

namespace FormBuilderWebApp.Models
{
    public enum Status
    {
        Online,
        Offline,
        Blocked
    }

    public class TestViewModel2
    {
        public string Friend { get; }

        public TestViewModel3 TestModel3 { get; }

        public TestViewModel2(string friend,TestViewModel3 testViewModel3)
        {
            Friend = friend;
            TestModel3 = testViewModel3;
        }
    }

    public class TestViewModel3
    {
        [DisplayName("Девушка")]
        public string Girlfriend { get; }

        public TestViewModel3(string girlfriend)
        {
            Girlfriend = girlfriend;
        }
    }

    [DisplayName("Your hero")]
    public class TestViewModel
    {
        public TestViewModel(int id, string name, TestViewModel2 testViewModel2, string battleCry,
             int amountOfChildren = 3, bool isOgre = true, Status status = Status.Online, decimal cash = 100)
        {
            Id = id;
            Name = name;
            BattleCry = battleCry;
            IsOgre = isOgre;
            Status = status;
            TestModel2 = testViewModel2;
            AmountOfChildren = amountOfChildren;
            Cash = cash;
        }

        public int Id { get; } = 1;

        [DisplayName("Имя")]
        public string Name { get; }

        public string BattleCry { get; }

        public bool IsOgre { get; }

        public Status Status { get; }

        public TestViewModel2 TestModel2 { get; }

        public int AmountOfChildren { get; }

        public decimal Cash { get; }
    }
}
