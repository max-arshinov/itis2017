﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Factory
{
    public class InputTypes
    {
        public Properties Properties { get; set; }
        public InputTypes(Properties properties) => this.Properties = properties;
    }
}
