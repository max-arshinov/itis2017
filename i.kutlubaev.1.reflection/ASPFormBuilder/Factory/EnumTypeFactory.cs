﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Factory
{
    public class EnumTypeFactory : InputTypesFactory
    {
        public EnumTypeFactory(Properties properties) : base(properties)
        {
        }

        public override InputTypes Create()
        {
            return new EnumType(Properties);
        }
    }
}
