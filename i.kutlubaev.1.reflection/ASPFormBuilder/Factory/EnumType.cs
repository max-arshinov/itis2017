﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Factory
{
    public class EnumType : InputTypes
    {
        public EnumType(Properties properties) : base(properties)
        {
        }

        public override string ToString()
        {
            var sb = "";
            sb += $"{Properties.Name}<select>";
            foreach(var name in Properties.EnumNames)
            {
                sb += $"<option value={name}>{name}</option>";
            }
            sb += "</select>";
            return sb;
        }

    }
}
