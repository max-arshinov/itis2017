﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Factory
{
    public abstract class InputTypesFactory
    {
        public Properties Properties { get; set; }

        public InputTypesFactory(Properties properties) => this.Properties = properties;

        public abstract InputTypes Create();
    }
}
