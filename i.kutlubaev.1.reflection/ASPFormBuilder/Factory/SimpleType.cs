﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Factory
{
    public class SimpleType : InputTypes
    {
        public SimpleType(Properties properties) : base(properties)
        {
        }

        public override string ToString()
        {
            if (Properties.Type == "checkbox")
            {
                var isChecked = Convert.ToBoolean(Properties.Value) ? "checked" : "";
                return $"{Properties.Name}<input type={Properties.Type} name={Properties.Name} {isChecked}/> <br>";
            }
            return $"{Properties.Name}<input type={Properties.Type} name={Properties.Name} value={Properties.Value} step={Properties.Step}/> <br>";
        }
    }
}
