﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Factory
{
    public class Properties
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public object Value { get; set; }
        public string Step { get; set; }
        public string[] EnumNames { get; set; }

        public Properties(string Name, string Type, object Value, string Step)
        {
            this.Name = Name;
            this.Type = Type;
            this.Value = Value;
            this.Step = Step;
            if (Step == "")
                this.Step = "any";
        }

        public Properties(string Name, string[] EnumNames)
        {
            this.Name = Name;
            this.EnumNames = EnumNames;
        }

    }
}
