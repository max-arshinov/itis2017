﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Factory
{
    public class SimpleTypeFactory : InputTypesFactory
    {
        public SimpleTypeFactory(Properties properties) : base(properties)
        {
        }

        public override InputTypes Create()
        {
            return new SimpleType(Properties);
        }

    }
}
