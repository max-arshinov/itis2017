﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASPFormBuilder.Models;

namespace ASPFormBuilder.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            Builder.Builder formBuilder = new Builder.FormBuilder();
            Builder.FormDirector formDirector = new Builder.FormDirector(formBuilder);
            Myclass myclass = new Myclass("qwe", 1, true);
            return View(model: formDirector.HtmlBuild(myclass));
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
