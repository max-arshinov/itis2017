﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Builder
{
    public class FormBuilder : Builder
    {
        HtmlFormBuilder hfb;
        public FormBuilder()
        {
            hfb = new HtmlFormBuilder();
        }
        public override string Build(object SomeClass)
        {
            var customClass = SomeClass.GetType();
            if (!customClass.IsClass)
                throw new ArgumentException("Not Class");

            var properties = customClass.GetProperties();

            foreach (var property in properties)
            {
                var attr = (DisplayNameAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayNameAttribute));
                string fieldName = attr?.DisplayName != null ? attr.DisplayName : property.Name;

                if (property.PropertyType.IsEnum)
                {
                    hfb.AddEnumInputs(property.PropertyType.GetEnumNames(), fieldName);
                    continue;
                }
                hfb.AddInput(property.PropertyType.Name, fieldName, SomeClass.GetType().GetProperty(property.Name).GetValue(SomeClass,null));
            }
            return hfb.sb;
        }
       

    }
}
