﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Builder
{
    public abstract class Builder
    {
        public abstract string Build(object SomeClass);
    }
}
