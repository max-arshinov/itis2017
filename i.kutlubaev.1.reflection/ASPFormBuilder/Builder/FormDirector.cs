﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPFormBuilder.Builder
{
    public class FormDirector
    {
        Builder FormBuilder;
        public FormDirector(Builder formBuilder)
        {
            this.FormBuilder = formBuilder;
        }

        public string HtmlBuild(object SomeClass)
        {
            return FormBuilder.Build(SomeClass);
        }

    }
}
