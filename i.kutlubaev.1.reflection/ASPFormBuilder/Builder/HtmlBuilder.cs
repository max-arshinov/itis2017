﻿using ASPFormBuilder.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPFormBuilder.Builder
{
    public class HtmlFormBuilder
    {
        public string sb { get; private set; }
        public HtmlFormBuilder()
        {
            this.sb = "";
        }

        public void AddInput(string type, string name, object value)
        {
            string thisType = "";
            string thisStep = "";
            switch (type)
            {
                case "Boolean":
                    thisType = "checkbox";
                    break;
                case "Int32":
                    thisType = "number";
                    thisStep = "1";
                    break;
                case "String":
                    thisType = "text";
                    break;
                case "Double":
                case "Single":
                case "Decimal":
                    thisType = "number";
                    break;
            }
            Append(new SimpleTypeFactory(new Properties(
                name, thisType, value, thisStep
                )));
        }

        private void Append(InputTypesFactory inputTypesFactory)
        {
            sb += inputTypesFactory.Create().ToString();
        }

        public void AddEnumInputs(string[] names, string fieldname)
        {
            Append(new EnumTypeFactory(new Properties(
                fieldname, names
                )));
        }
        
    }
}
