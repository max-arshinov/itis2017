﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Singleton
{
    /// <summary>
    /// потокобезопасная, т.к. статические конструкторы в C# 
    /// вызываются для выполнения только тогда, 
    /// когда создается экземпляр класса или ссылается на статический член класса, 
    /// и выполняются только один раз
    /// </summary>
    public sealed class Singleton
    {
        private static readonly Singleton singleton = new Singleton();
        public string SomeKek { get; set; }
        /// <summary>
        /// не помечаем как beforefieldinit
        /// </summary>
        static Singleton() { }

        private Singleton() => SomeKek = "ReallyKek";

        public static Singleton GetSingleton() => singleton;
    }



    class Program
    {
        static void Main(string[] args)
        {
            (new Thread(() =>
            {
                var SingletonField = Singleton.GetSingleton().SomeKek;
            }))
            .Start();
            Console.WriteLine($"Hello {Singleton.GetSingleton().SomeKek}!");
        }
    }
}
