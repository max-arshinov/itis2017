﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace WebCrawler
{
    public class Storage
    {
        private static IList<string> _linksStorage;

        private Storage() { }

        public static IList<string> GetStorage()
        {
            return _linksStorage ?? (_linksStorage = new List<string>());
        }

        public static void Initialize(IList<string> links)
        {
            if (_linksStorage == null || !(_linksStorage as IReadOnlyCollection<string>).Any())
                _linksStorage = links;
        }
    }
}
