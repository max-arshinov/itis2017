﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebCrawler.Factory;

namespace WebCrawler
{
    public enum UrlType
    {
        Html,
        Xml,
        Gzip
    }

    public class Crawler
    {
        private readonly HttpClient _client;

        public int MaxUrls { get; }

        public Crawler(int maxUrls)
        {
            MaxUrls = maxUrls;
            _client = new HttpClient();
        }

        public async Task StartCrawl(string robotTxtPath)
        {
            var data = Storage.GetStorage();
            if (!data.CheckNullOrEmpty())
                return;

            Console.WriteLine("Идет процесс получения данных...");

            var rootSitemap = await GetRootSitemapUrl(robotTxtPath)
                .ConfigureAwait(false);

            var sitemaps = await GetSitemaps(rootSitemap)
                .ConfigureAwait(false);

            var parsedUrls = await CrawlRecursive(sitemaps, new List<string>() { Capacity = MaxUrls }).ConfigureAwait(false);
            Storage.Initialize(parsedUrls);

            Console.WriteLine("Данные получены. Для приостановления обработки нажмите N");
            Console.WriteLine("Для отмены нажмите Escape");
        }

        private async Task<List<string>> CrawlRecursive(IReadOnlyCollection<string> sitemaps, List<string> linksStorage)
        {
            var firstUrl = sitemaps.FirstOrDefault();
            var urlType = GetUrlType(firstUrl);

            if (urlType == UrlType.Html)
            {
                var freePlace = linksStorage.Capacity - linksStorage.Count;

                if (freePlace >= sitemaps.Count)
                    linksStorage = linksStorage.Concat(sitemaps).ToList();
                else
                    linksStorage = linksStorage.Concat(sitemaps.Take(freePlace)).ToList();

                Console.WriteLine(linksStorage.Count);
                return linksStorage;
            }

            foreach (var url in sitemaps)
            {
                if (linksStorage.Count == MaxUrls)
                    return linksStorage;

                urlType = GetUrlType(url);

                var loadedLinks = await GetUrlsFromXml(url, urlType).ConfigureAwait(false); ;

                linksStorage = await CrawlRecursive(loadedLinks, linksStorage).ConfigureAwait(false); ;
            }

            return linksStorage;
        }

        private async Task<string> GetRootSitemapUrl(string path)
        {
            var regex = new Regex(@"Sitemap:(.)*");

            var response = await _client.GetStringAsync(path).ConfigureAwait(false);

            var rootSitemapLink = regex.Match(response).ToString().Replace("Sitemap:", string.Empty);

            return rootSitemapLink;
        }

        private async Task<IReadOnlyCollection<string>> GetSitemaps(string rootSitemapUrl)
        {
            var urlType = GetUrlType(rootSitemapUrl);

            return await GetUrlsFromXml(rootSitemapUrl, urlType)
                .ConfigureAwait(false);
        }

        private UrlType GetUrlType(string url)
        {
            if (url.Contains(".gz"))
                return UrlType.Gzip;

            return url.Contains(".xml") ? UrlType.Xml : UrlType.Html;
        }

        private async Task<IReadOnlyCollection<string>> GetUrlsFromXml(string path, UrlType urlType)
        {
            var response = await _client.GetStreamAsync(path).ConfigureAwait(false); ;

            XDocument xml;

            if (urlType == UrlType.Gzip)
            {
                var gzipDecompressed = new GZipStream(response, CompressionMode.Decompress);
                xml = XDocument.Load(gzipDecompressed);
            }
            else
                xml = XDocument.Load(response);

            var rootNamespace = xml.Root?.GetDefaultNamespace();
            return xml.Descendants(rootNamespace + "loc").Select(x => x.Value).ToList();
        }
    }
}

