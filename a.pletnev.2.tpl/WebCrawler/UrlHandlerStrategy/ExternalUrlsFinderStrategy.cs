﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace WebCrawler.UrlHandlerStrategy
{
    public class ExternalUrlsFinderStrategy : IHandlerStrategy
    {
        private readonly string _domain;

        public ExternalUrlsFinderStrategy(string domain)
        {
            _domain = domain;
        }

        public async Task<IList<string>> Execute(string url)
        {
            // Так как метод sendAsync не потокобезопасный, создаю по эксземпляру на "поток".
            var httpClient = new HttpClient();

            var response = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, url))
                .ConfigureAwait(false);

            if ((int)response.StatusCode != 200)
                return null;

            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            var htmlDoc = new HtmlDocument();

            htmlDoc.LoadHtml(content);

            var hrefUrls = htmlDoc.DocumentNode.Descendants("a")
                .Select(x => x.GetAttributeValue("href", null))
                .Where(x => !string.IsNullOrEmpty(x) && x.Contains(_domain))
                .Select(CorrectUrl)
                .ToList();

            var httpCodes = new List<int>();

            foreach (var hrefLink in hrefUrls)
            {
                var tmp = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, hrefLink));
                httpCodes.Add((int)tmp.StatusCode);
            }

            var urlsAndCodes = hrefUrls.Zip(httpCodes, (hrefUrl, code) => hrefUrl + "," + code)
                .ToList();

            return urlsAndCodes;
        }

        private string CorrectUrl(string url)
        {
            if (!url.Contains("https"))
                url = "https:" + url;

            return url;
        }
    }
}
