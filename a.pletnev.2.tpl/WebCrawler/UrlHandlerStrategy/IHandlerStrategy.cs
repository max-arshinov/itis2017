﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebCrawler.UrlHandlerStrategy
{
    public interface IHandlerStrategy
    {
         Task<IList<string>> Execute(string htmlLink);
    }
}
