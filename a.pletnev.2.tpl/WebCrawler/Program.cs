﻿using System;
using System.Threading;
using System.Threading.Tasks;
using WebCrawler.Factory;
using WebCrawler.UrlHandlerStrategy;

namespace WebCrawler
{
    public class Program
    {
        public static int LastIterationNumber = 0;
        public static int HandledData = 0;

        static void Main(string[] args)
        {
            Console.WriteLine("домен сайта:");
            var domain = Console.ReadLine();

            Console.WriteLine("Максимальное количество обрабатываемых ссылок:");
            var maxUrls = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Количество одновременных подключений:");
            var concurrentСonnections = Convert.ToInt32(Console.ReadLine());

            while (true)
            {
                var cts = new CancellationTokenSource();
                var token = cts.Token;

                var fileLoggerFactory = new FileLoggerFactory(@"D:\test.csv");
                var crawler = new Crawler(maxUrls);
                var urlsHandler = new UrlsHandler(concurrentСonnections, token, fileLoggerFactory);
                var externalFinder = new ExternalUrlsFinderStrategy(domain);

                var facade = new Facade(crawler, urlsHandler, externalFinder);

                var task = Task.Run(() => facade.CrawlAndHandle($"https://www.{domain}/robots.txt"), token);

                while (Storage.GetStorage().CheckNullOrEmpty()) { }
             
                var consoleKey = default(ConsoleKey);

                while (consoleKey != ConsoleKey.N)
                {
                    consoleKey = Console.ReadKey(true).Key;

                    if (consoleKey == ConsoleKey.Escape)
                        return;
                }

                cts.Cancel();

                Console.WriteLine("Останавливаем обработку...");
                task.Wait();

                consoleKey = default(ConsoleKey);

                Console.WriteLine("Для продолжения обработки нажмите Enter");

                while (consoleKey != ConsoleKey.Enter)
                {
                    consoleKey = Console.ReadKey(true).Key;
                    if (consoleKey == ConsoleKey.Escape)
                        return;
                }

                Console.WriteLine("Продолжение обработки...");
                Console.WriteLine("Для приостановления обработки нажмите N");
                Console.WriteLine("Для отмены нажмите Escape");
            }
        }
    }
}
