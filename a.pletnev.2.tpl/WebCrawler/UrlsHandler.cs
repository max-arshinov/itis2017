﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebCrawler.UrlHandlerStrategy;
using WebCrawler.Factory;

namespace WebCrawler
{
    public class UrlsHandler
    {
        public int ConcurrentСonnections { get; }
        private readonly CancellationToken _token;
        private readonly Logger _logger;

        public UrlsHandler(int concurrentСonnections, CancellationToken token, LoggerFactory logger)
        {
            ConcurrentСonnections = concurrentСonnections;
            _token = token;
            _logger = logger.Create();
        }

        public async Task ParallelHandleUrls(IList<string> unprocessedUrls, IHandlerStrategy handlerStrategy)
        {
            var interations = unprocessedUrls.Count / ConcurrentСonnections + 1;
            var currentInterationNumber = 0;

            while (currentInterationNumber < interations)
            {
                var targetLinks = new List<Task<IList<string>>>();

                var startIndex = currentInterationNumber * ConcurrentСonnections;
                var endIndex = startIndex + ConcurrentСonnections;

                // Сохраняем кол-во обработанных данных.
                Program.HandledData = startIndex;

                for (var i = startIndex; i < endIndex; i++)
                {
                    targetLinks.Add(handlerStrategy.Execute(unprocessedUrls[i]));
                }

                var tasksResult = await Task.WhenAll(targetLinks).ConfigureAwait(false);

                var externalUrls = new HashSet<string>();

                foreach (var result in tasksResult)
                {
                    externalUrls.UnionWith(result);
                }

                var resultRows = externalUrls.Aggregate((x, y) => x + (y + '\n'));

                await _logger.Log(resultRows).ConfigureAwait(false);

                currentInterationNumber++;

                if (!_token.IsCancellationRequested) continue;

                Program.LastIterationNumber = currentInterationNumber;

                Console.WriteLine("Обработка остановлена.Нажмите Enter для продолжения");
                return;
            }

            Console.WriteLine("Данные успешно обработаны");
            Environment.Exit(0);
        }
    }
}
