﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.Factory
{
    public class ConsoleLoggerFactory : LoggerFactory
    {
        private readonly string _message;

        public ConsoleLoggerFactory(string message)
        {
            _message = message;
        }

        public override Logger Create() => new ConsoleLogger();
    }
}
