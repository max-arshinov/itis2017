﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebCrawler.Factory
{
    public abstract class Logger
    {
        public abstract Task Log(string data);
    }
}
