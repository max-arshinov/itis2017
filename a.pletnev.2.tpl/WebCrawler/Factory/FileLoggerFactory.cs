﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebCrawler.Factory
{
    public class FileLoggerFactory : LoggerFactory
    {
        private readonly string _path;

        public FileLoggerFactory(string path)
        {
            _path = path;
        }

        public override Logger Create() => new FileLogger(_path);
    }
}
