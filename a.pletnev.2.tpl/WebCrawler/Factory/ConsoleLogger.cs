﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler.Factory
{
    /// <summary>
    /// Не использую, но можно заменить вместо лога в файл.
    /// </summary>
    public class ConsoleLogger : Logger
    {
        public override Task Log(string message)
        {
           return Task.Run(() => Console.WriteLine(message));
        }
    }
}
