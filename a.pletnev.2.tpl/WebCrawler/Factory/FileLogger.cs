﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler.Factory
{
    public class FileLogger : Logger
    {
        private readonly string _path;

        public FileLogger(string path)
        {
            _path = path;
        }

        public override async Task Log(string data)
        {
            using (var fs = new FileStream(_path, FileMode.OpenOrCreate))
            {
                var buffer = Encoding.UTF8.GetBytes(data);

                await fs.WriteAsync(buffer, 0, buffer.Length).ConfigureAwait(false); ;
            }
        }
    }
}
