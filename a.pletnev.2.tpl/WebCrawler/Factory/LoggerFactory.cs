﻿namespace WebCrawler.Factory
{
    public abstract class LoggerFactory
    {
        public abstract Logger Create();
    }
}
