﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebCrawler
{
    public static class Extensions
    {
        public static bool CheckNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || !(source as IReadOnlyCollection<T>).Any();
        }
    }
}
