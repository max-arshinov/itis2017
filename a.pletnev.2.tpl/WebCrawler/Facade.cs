﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebCrawler.Factory;
using WebCrawler.UrlHandlerStrategy;

namespace WebCrawler
{
    public class Facade
    {
        private readonly Crawler _crawler;
        private readonly UrlsHandler _urlsHandler;
        private readonly IHandlerStrategy _handlerStrategy;



        public Facade(Crawler crawler, UrlsHandler urlsHandler, IHandlerStrategy handlerStrategy)
        {
            _crawler = crawler;
            _urlsHandler = urlsHandler;
            _handlerStrategy = handlerStrategy;
        }

        public async Task CrawlAndHandle(string path)
        {
            await _crawler.StartCrawl(path);

            var unprocessedUrls = Storage.GetStorage()
                .Skip(Program.LastIterationNumber * _urlsHandler.ConcurrentСonnections)
                .ToList();

            await _urlsHandler.ParallelHandleUrls(unprocessedUrls, _handlerStrategy);
        }
    }
}
